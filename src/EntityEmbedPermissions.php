<?php

namespace Drupal\entity_embed_permissions;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\entity_embed\EntityEmbedDisplay\EntityEmbedDisplayManager;

/**
 * Provides dynamic permissions for each entity type.
 */
class EntityEmbedPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity type repository service.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface
   */
  protected $entityTypeRepository;

  /**
   * The Entity Embed Display plugin manager.
   *
   * @var \Drupal\entity_embed\EntityEmbedDisplay\EntityEmbedDisplayManager
   */
  protected $displayPluginManager;

  /**
   * MediaPermissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityTypeRepositoryInterface $entity_type_repository, EntityEmbedDisplayManager $display_plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeRepository = $entity_type_repository;
    $this->displayPluginManager = $display_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.repository'),
      $container->get('plugin.manager.entity_embed.display')
    );
  }

  /**
   * Returns an array of media type permissions.
   *
   * @return array
   *   The media type permissions.
   *
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function entityTypePermissions() {
    $perms = [];
    $entity_types = $this->getEntityTypeOptions();
    foreach ($entity_types as $entity_name => $label) {
      $perms["view embedded $entity_name"] = [
        'title' => $this->t('View embedded %entity_label', ['%entity_label' => $label]),
      ];
    }
    return $perms;
  }

  /**
   * Builds a list of entity types.
   *
   * Configuration entity types without a view builder are filtered out while
   * all other entity types are kept.
   *
   * @return array
   *   An array of entity type labels, keyed by entity type name.
   */
  protected function getEntityTypeOptions() {
    $options = $this->entityTypeRepository->getEntityTypeLabels();

    foreach ($options as $entity_type_id => $entity_type_label) {
      // Filter out entity types that do not have a view builder class.
      if (!$this->entityTypeManager->getDefinition($entity_type_id)->hasViewBuilderClass()) {
        unset($options[$entity_type_id]);
      }
      // Filter out entity types that do not support UUIDs.
      elseif (!$this->entityTypeManager->getDefinition($entity_type_id)->hasKey('uuid')) {
        unset($options[$entity_type_id]);
      }
      // Filter out entity types that will not have any Entity Embed Display
      // plugins.
      elseif (!$this->displayPluginManager->getDefinitionOptionsForEntityType($entity_type_id)) {
        unset($options[$entity_type_id]);
      }
    }

    return $options;
  }

}
